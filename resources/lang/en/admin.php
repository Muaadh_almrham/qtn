<?php

return [
    'branch' => [
        'title' => 'Branches',

        'actions' => [
            'index' => 'Branches',
            'create' => 'New Branch',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'location' => 'Location',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];